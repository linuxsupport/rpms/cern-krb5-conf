Name:      cern-krb5-conf
Version:   1.3
Release:   7%{?dist}
Group:     CERN/Utilities
License:   BSD
URL:       http://linux.cern.ch
Source0:   %{name}-%{version}.tgz
BuildArch: noarch
Requires:  krb5-libs, cern-krb5-conf-defaults-cernch, cern-krb5-conf-realm-cernch
Summary:   A kerberos configuration file to be used at CERN

%description
This rpm provides a basic kerberos configuration file to be used at CERN

%package atlas
Requires: krb5-libs, cern-krb5-conf-defaults-cernch, cern-krb5-conf-realm-cernch-atlas
Summary: A kerberos configuration file to be used at CERN (ATLAS)
%description atlas
This rpm provides a basic kerberos configuration file to be used at CERN (ATLAS)

%package cms
Requires: krb5-libs, cern-krb5-conf-defaults-cernch, cern-krb5-conf-realm-cernch-cms
Summary: A kerberos configuration file to be used at CERN (CMS)
%description cms
This rpm provides a basic kerberos configuration file to be used at CERN (CMS)

%package tn
Requires: krb5-libs, cern-krb5-conf-defaults-cernch, cern-krb5-conf-realm-cernch-tn
Summary: A kerberos configuration file to be used at CERN (TN)
%description tn
This rpm provides a basic kerberos configuration file to be used at CERN (TN)

%package ipadev
Requires: krb5-libs, cern-krb5-conf-defaults-ipadev, cern-krb5-conf-realm-ipadev
Summary: A Kerberos configuration file to be used for CERN's FreeIPA development realm
%description ipadev
This RPM provides a basic Kerberos configuration file to be used for CERN's 
FreeIPA development realm

%package defaults-cernch
Summary: CERN's default configuration for CERN.CH realm
Requires: krb5-libs
Conflicts: cern-krb5-conf-defaults-ipadev
%description defaults-cernch
CERN's global settings for CERN.CH realm

%package defaults-ipadev
Summary: CERN's default configuration for IPA-DEV.CERN.CH realm
Requires: krb5-libs
Conflicts: cern-krb5-conf-defaults-cernch
%description defaults-ipadev
CERN's global settings for IPA-DEV.CERN.CH realm

%package realm-cernch
Summary: CERN's parameters for CERN.CH realm
Requires: krb5-libs
Conflicts: cern-krb5-conf-realm-cernch-atlas, cern-krb5-conf-realm-cernch-cms, cern-krb5-conf-realm-cernch-tn
%description realm-cernch
CERN's parameters for CERN.CH realm

%package realm-cernch-atlas
Summary: CERN's parameters for CERN.CH realm on ATLAS network
Requires: krb5-libs
Conflicts: cern-krb5-conf-realm-cernch, cern-krb5-conf-realm-cernch-cms, cern-krb5-conf-realm-cernch-tn
%description realm-cernch-atlas
CERN's parameters for CERN.CH realm on ATLAS network

%package realm-cernch-cms
Summary: CERN's parameters for CERN.CH realm on CMS network
Requires: krb5-libs
Conflicts: cern-krb5-conf-realm-cernch, cern-krb5-conf-realm-cernch-atlas, cern-krb5-conf-realm-cernch-tn
%description realm-cernch-cms
CERN's parameters for CERN.CH realm on CMS network

%package realm-cernch-tn
Summary: CERN's parameters for CERN.CH realm on TN network
Requires: krb5-libs
Conflicts: cern-krb5-conf-realm-cernch, cern-krb5-conf-realm-cernch-atlas, cern-krb5-conf-realm-cernch-cms
%description realm-cernch-tn
CERN's parameters for CERN.CH realm on TN network

%package realm-ipadev
Summary: CERN's parameters for IPA-DEV.CERN.CH realm
Requires: krb5-libs
%description realm-ipadev
CERN's parameters for IPA-DEV.CERN.CH realm

%prep
%setup -q

%build
%install

install -d %{buildroot}/%{_sysconfdir}/krb5.conf.d
install -p -m 0644 cern-defaults-cernch.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-defaults-cernch.conf
%if 0%{?rhel} >= 8
install -p -m 0644 cern-defaults-dns_canon_host_fallback.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-defaults-dns_canon_host_fallback.conf
%endif
install -p -m 0644 cern-defaults-ipadev.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-defaults-ipadev.conf
install -p -m 0644 cern-realm-cernch.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-realm-cernch.conf
install -p -m 0644 cern-realm-cernch-atlas.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-realm-cernch-atlas.conf
install -p -m 0644 cern-realm-cernch-cms.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-realm-cernch-cms.conf
install -p -m 0644 cern-realm-cernch-tn.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-realm-cernch-tn.conf
install -p -m 0644 cern-realm-ipadev.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-realm-ipadev.conf

%files defaults-cernch
%{_sysconfdir}/krb5.conf.d/cern-defaults-cernch.conf
%if 0%{?rhel} >= 8
%{_sysconfdir}/krb5.conf.d/cern-defaults-dns_canon_host_fallback.conf
%endif

%files defaults-ipadev
%{_sysconfdir}/krb5.conf.d/cern-defaults-ipadev.conf
%if 0%{?rhel} >= 8
%{_sysconfdir}/krb5.conf.d/cern-defaults-dns_canon_host_fallback.conf
%endif

%files realm-cernch
%{_sysconfdir}/krb5.conf.d/cern-realm-cernch.conf

%files realm-cernch-atlas
%{_sysconfdir}/krb5.conf.d/cern-realm-cernch-atlas.conf

%files realm-cernch-cms
%{_sysconfdir}/krb5.conf.d/cern-realm-cernch-cms.conf

%files realm-cernch-tn
%{_sysconfdir}/krb5.conf.d/cern-realm-cernch-tn.conf

%files realm-ipadev
%{_sysconfdir}/krb5.conf.d/cern-realm-ipadev.conf

%files
%files atlas
%files cms
%files tn
%files ipadev

%changelog
* Wed Jun 23 2021 Julien Rische <julien.rische@cern.ch> 1.3-7
- Do not configure dns_canonicalize_hostname on CC7

* Tue Jun 01 2021 Julien Rische <julien.rische@cern.ch> 1.3-6
- Enable reverse DNS resolution for CERN.CH realm
- Move IPAdev domain/realm mapping to realm subpackage

* Tue May 18 2021 Julien Rische <julien.rische@cern.ch> 1.3-5
- Configure fallback mode for DNS hostname canonicalisation on C8

* Tue Apr 27 2021 Daniel Juarez <djuarezg@cern.ch> 1.3-4
- Make tickets proxiable by default

* Wed Jan 20 2021 Julien Rische <julien.rische@cern.ch> 1.3-3
- Enable DNS reverse resolution in IPAdev defaults

* Tue Dec 15 2020 Julien Rische <julien.rische@cern.ch> 1.3-2
- Make IPAdev defaults converge with current AD ones

* Wed Dec 02 2020 Julien Rische <julien.rische@cern.ch> 1.3-1
- Make RPM-based Kerberos configuration more modular

* Mon May 11 2020 Julien Rische <julien.rische@cern.ch> 1.2-1
- Add subpackage for FreeIPA development realm

* Tue Jan 28 2020 Ben Morrice <ben.morrice@cern.ch> 1.1-1
- add subpackages to provide krb5.conf for atlas,cms,tn

* Tue Jan 14 2020 Ben Morrice <ben.morrice@cern.ch> 1.0-1
- Initial release
