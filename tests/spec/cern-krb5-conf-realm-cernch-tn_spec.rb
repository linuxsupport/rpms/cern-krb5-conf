require 'spec_helper'

describe package('cern-krb5-conf-realm-cernch-tn') do
  it { should be_installed }
end

describe package('cern-krb5-conf-realm-cernch') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-realm-cernch-atlas') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-realm-cernch-cms') do
  it { should_not be_installed }
end

describe file('/etc/krb5.conf.d/cern-realm-cernch-tn.conf') do
  it { should exist }
  it { should be_file }
  it { should be_owned_by 'root' }
  it { should be_grouped_into 'root' }
  it { should be_mode 644 }
end
