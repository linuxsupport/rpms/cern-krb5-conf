require 'spec_helper'

describe package('cern-krb5-conf-realm-ipadev') do
  it { should be_installed }
end

describe file('/etc/krb5.conf.d/cern-realm-ipadev.conf') do
  it { should exist }
  it { should be_file }
  it { should be_owned_by 'root' }
  it { should be_grouped_into 'root' }
  it { should be_mode 644 }
end
