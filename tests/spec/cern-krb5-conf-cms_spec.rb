require 'spec_helper'

describe package('cern-krb5-conf-cms') do
  it { should be_installed }
end

describe package('cern-krb5-conf') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-atlas') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-tn') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-ipadev') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-defaults-cernch') do
  it { should be_installed }
end

describe package('cern-krb5-conf-realm-cernch-cms') do
  it { should be_installed }
end

describe package('cern-krb5-conf-realm-cernch') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-realm-cernch-atlas') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-realm-cernch-tn') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-defaults-ipadev') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-realm-ipadev') do
  it { should_not be_installed }
end

describe file('/etc/krb5.conf.d/cern-defaults-cernch.conf') do
  it { should exist }
  it { should be_file }
  it { should be_owned_by 'root' }
  it { should be_grouped_into 'root' }
  it { should be_mode 644 }
end

describe file('/etc/krb5.conf.d/cern-realm-cernch-cms.conf') do
  it { should exist }
  it { should be_file }
  it { should be_owned_by 'root' }
  it { should be_grouped_into 'root' }
  it { should be_mode 644 }
end

describe file('/etc/krb5.conf.d/cern-defaults-dns_canon_host_fallback.conf') do
  if os[:release].match('^7\.')
    it { should_not exist }
  else
    it { should exist }
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    it { should be_mode 644 }
  end
end

describe file('/etc/krb5.conf.d/cern-realm-cernch.conf') do
  it { should_not exist }
end

describe file('/etc/krb5.conf.d/cern-realm-cernch-atlas.conf') do
  it { should_not exist }
end

describe file('/etc/krb5.conf.d/cern-realm-cernch-tn.conf') do
  it { should_not exist }
end

describe file('/etc/krb5.conf.d/cern-defaults-ipadev.conf') do
  it { should_not exist }
end

describe file('/etc/krb5.conf.d/cern-realm-ipadev.conf') do
  it { should_not exist }
end
