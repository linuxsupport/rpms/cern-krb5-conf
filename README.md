# cern-krb5-conf

* This is a simple rpm to package and distribute /etc/krb5.conf.d/cern.conf
* The default config in /etc/krb5.conf has "includedir /etc/krb5.conf.d/" in SLC6/CC7 and C8
* As this package does not overwrite /etc/krb5.conf, it's safe to be used on all hosts (however, it may not be referenced if the default krb5.conf has been modified to remove "includedir")
* As we have different domain controllers at CERN based on physical network locality, this package also contains several subpackages that define configuration specific to that area. These subpackages being:
	* cern-krb5-conf-atlas
	* cern-krb5-conf-cms
	* cern-krb5-conf-tn
* A configuration for CERN's FreeIPA development realm is also provided. It still contains realm information from krb5-conf, but IPA-DEV.CERN.CH realm is set as default:
	* cern-krb5-conf-ipadev
